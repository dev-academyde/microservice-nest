import { Field, InputType } from 'type-graphql';
import { IsEmail, IsNotEmpty } from 'class-validator';

@InputType()
export class CreateUserArgs {
  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsNotEmpty()
  password: string;
}
