import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Transport } from '@nestjs/common/enums/transport.enum';
import { AppResolver } from './app.resolver';

@Module({
  imports: [
    ClientsModule.register([
      { name: 'user_service', transport: Transport.TCP },
    ]),
    GraphQLModule.forRoot({
      playground: true,
      autoSchemaFile: 'schema.gql',
      installSubscriptionHandlers: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService, AppResolver],
})
export class AppModule {}
