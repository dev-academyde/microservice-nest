import { Resolver, Query, Mutation, Args, Subscription } from '@nestjs/graphql';
import { Inject, UsePipes, ValidationPipe } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateUserArgs } from './Schema/CreateUserArgs';
import { PubSub } from 'graphql-subscriptions';

const pubSub = new PubSub();

@Resolver()
@UsePipes(ValidationPipe)
export class AppResolver {
  constructor(@Inject('user_service') private readonly client: ClientProxy) {}

  @Query(returns => String)
  test() {
    return 'Hello World';
  }

  @Mutation(returns => Boolean)
  async createUser(@Args('input') payload: CreateUserArgs) {
    const user = await this.client
      .send({ cmd: 'create_user' }, payload)
      .toPromise();

    console.log('User', user);

    return true;
  }

  @Mutation(returns => Boolean)
  testSubscription(
    @Args({ name: 'message', type: () => String }) message: string,
  ) {
    pubSub.publish('userAdded', { userAdded: message });
    return true;
  }

  @Subscription(returns => String)
  userAdded() {
    return pubSub.asyncIterator('userAdded');
  }
}
