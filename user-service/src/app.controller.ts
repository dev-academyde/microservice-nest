import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UsePipes,
  Inject,
} from '@nestjs/common';
import { AppService } from './app.service';
import { CreateUserDto } from './Dto/CreateUserDto';
import { ClientProxy } from '@nestjs/microservices';

@Controller()
@UsePipes(ValidationPipe)
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('user_service') private readonly client: ClientProxy,
  ) {}

  @Post('create-user')
  async createUser(@Body() payload: CreateUserDto) {
    const user = await this.client
      .send({ cmd: 'create_user' }, payload)
      .toPromise();

    this.client.emit('user_created', user);

    return user;
  }
}
