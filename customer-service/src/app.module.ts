import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Transport } from '@nestjs/common/enums/transport.enum';

@Module({
  imports: [
    ClientsModule.register([
      { name: 'user_service', transport: Transport.TCP },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
