import { Controller } from '@nestjs/common';
import { MessagePattern, EventPattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  @MessagePattern({ cmd: 'create_user' })
  async createUser(payload) {
    console.log('Create user', payload);

    return payload;
  }

  @EventPattern('user_created')
  async handleUserCreated(payload) {
    console.log('User created', payload);
  }
}
